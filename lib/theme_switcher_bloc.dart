library theme_switcher_bloc;

import 'dart:async';

class ThemeSwitcherBloc {

  bool _isLight;
  StreamController<bool> _controller = StreamController.broadcast();

  ThemeSwitcherBloc([bool isLight]): _isLight = isLight ?? true;

  bool get isLight => _isLight;
  Stream<bool> get listen => _controller.stream;

  void change() {
    _isLight = !_isLight;
    _controller.sink.add(_isLight);
  }

  void dispose() {
    _controller.close();
    _controller = null;
  }

}
