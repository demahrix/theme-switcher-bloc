import 'dart:async';

import 'package:test/test.dart';
import 'package:theme_switcher_bloc/theme_switcher_bloc.dart';

void main() {

  test("switch theme", () async {

    final Completer<void> completer = Completer();

    ThemeSwitcherBloc bloc = ThemeSwitcherBloc();

    expect(true, bloc.isLight);

    bloc.listen.listen((event) {
      expect(false, event);
      completer.complete();
    });

    bloc.change();

    return completer.future;
  });
}
